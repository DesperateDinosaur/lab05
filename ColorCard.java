abstract class ColorCard implements Card {
    protected String color;
    protected String type;

    public ColorCard(String color, String type){
        this.color = color;
        this.type = type;
    }

    public String getColor(Card card){
        return this.color;
    }
}