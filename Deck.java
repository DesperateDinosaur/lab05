import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

import java.util.*;

public class Deck {
    //Create deck
    ArrayList<Card> deck = new ArrayList<Card>();
    public Deck() {
        //Add 8 total wild cards, 4 color change and 4 pick up four
        for (int i = 0; i < 2; i++){
            for (int j = 0; j < 4; j++){
                if (i == 0){
                    ChangeColorCard wildCard = new ChangeColorCard("colorWild");
                    deck.add(wildCard);
                }
                else if(i == 1){
                    PickFourCard fourCard = new PickFourCard("fourWild");
                    deck.add(fourCard);
                }
            }
        }

        //Add 104 total colored cards: 80 colored number cards and 24 colored special cards
        for (int i = 0; i < 4; i++){
            for (int j = 0; j < 2; j++){
                for (int k = 0; k < 10; k++){
                    //Create red numbered cards
                    if (i == 0){
                        NumberCard numCard = new NumberCard(k, "red", "number");

                        deck.add(numCard);
                    }
                    //Create yellow numbered cards
                    else if (i == 1){
                        NumberCard numCard = new NumberCard(k, "yellow", "number");

                        deck.add(numCard);
                    }
                    //Create green numbered cards
                    else if (i == 2){
                        NumberCard numCard = new NumberCard(k, "green", "number");

                        deck.add(numCard);
                    }
                    //Create blue numbered cards
                    else if (i == 3){
                        NumberCard numCard = new NumberCard(k, "blue", "number");

                        deck.add(numCard);
                    }
                }
                //Create red special cards
                if (i == 0){
                    SkipCard skipCard = new SkipCard("red", "skip");
                    ReverseCard revCard = new ReverseCard("red", "reverse");
                    PickTwoCard twoCard = new PickTwoCard("red", "pickTwo");

                    deck.add(skipCard);
                    deck.add(revCard);
                    deck.add(twoCard);
                }
                //Create yellow special cards
                else if (i == 1){
                    SkipCard skipCard = new SkipCard("yellow", "skip");
                    ReverseCard revCard = new ReverseCard("yellow", "reverse");
                    PickTwoCard twoCard = new PickTwoCard("yellow", "pickTwo");

                    deck.add(skipCard);
                    deck.add(revCard);
                    deck.add(twoCard);
                }
                //Create green special cards
                else if (i == 2){
                    SkipCard skipCard = new SkipCard("green", "skip");
                    ReverseCard revCard = new ReverseCard("green", "reverse");
                    PickTwoCard twoCard = new PickTwoCard("green", "pickTwo");

                    deck.add(skipCard);
                    deck.add(revCard);
                    deck.add(twoCard);
                }
                //Create blue special cards
                else if (i == 3){
                    SkipCard skipCard = new SkipCard("blue", "skip");
                    ReverseCard revCard = new ReverseCard("blue", "reverse");
                    PickTwoCard twoCard = new PickTwoCard("blue", "pickTwo");

                    deck.add(skipCard);
                    deck.add(revCard);
                    deck.add(twoCard);
                }
            }
        }
    }

    @Test
    public void addToDeck(Card card){
        //Add card to deck
        deck.add(card);

        //Since the deck is initialized with 112 cards, adding one should make 113
        assertEquals(113, deck.size());
    }

    @Test
    public Card draw(){
        //Fetch the index of the last card then using the index to store the card in a variable
        int last = deck.size() - 1;
        Card card = deck.get(last);

        //Remove the card from the deck and returning the stored card
        deck.remove(last);
        return card;
    }

    @Test
    public void shuffle(){
        //Iterate through deck ArrayList
        for (int i = 0; i < deck.size(); i++){
            //Generate random number
            Random rand = new Random();
            int max = deck.size();
            int randNum = rand.nextInt(max);

            //Create variables to store cards to be swapped
            Card one = deck.get(i);
            Card two = deck.get(randNum);

            //Perform the swap
            deck.set(i, two);
            deck.set(randNum, one);
        }
    }
}
