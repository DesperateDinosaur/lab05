abstract class WildCard implements Card {
    protected String activeColor;
    protected String type;

    public WildCard(String type){
        this.type = type;
    }

    public void setColor(String color){
        this.activeColor = color;
    };
}
