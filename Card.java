interface Card {
    abstract boolean canPlay(Card card);
}