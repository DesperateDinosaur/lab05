public class ReverseCard extends ColorCard{
    public ReverseCard(String color, String type){
        super(color, type);
    }

    public boolean canPlay(Card card){
        if (getColor(card) == this.color){
            return true;
        }
        else{
            return false;
        }
    }
}
