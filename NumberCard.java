public class NumberCard extends ColorCard{
    protected int number;

    public NumberCard(int number, String color, String type){
        super(color, type);
        this.number = number;
    }

    public boolean canPlay(Card card){
        if (getNumber(card) == this.number || getColor(card) == this.color){
            return true;
        }
        else{
            return false;
        }
    }

    public int getNumber(Card card){
        return this.number;
    }
}